from flask import Flask

app = Flask('zarupadev01')


@app.route('/')
def hello_world():
    return 'Naber Dünya?'

@app.route('/who')
def who():
    return 'Erhan is around!'

if __name__ == '__main__':
    app.run()
